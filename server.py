from tf.app import create_app
from tf.local_config import LocalConfig

app = create_app(LocalConfig)
