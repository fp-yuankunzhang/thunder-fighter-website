# -*- coding: utf-8 -*-
import os
from pprint import pprint

ROOT_DIR = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
activate_this = os.path.join(ROOT_DIR, 'venv/bin/activate_this.py')
execfile(activate_this, dict(__file__=activate_this))

from flask.ext.script import Manager
from flask_debugtoolbar import DebugToolbarExtension

from tf.app import create_app
from tf.local_config import LocalConfig

app = create_app(LocalConfig)
if app.config['DEBUG']:
    toolbar = DebugToolbarExtension(app)
manager = Manager(app)


@manager.shell
def make_shell_context():
    '''Make a shell context'''
    return dict(app=app)


@manager.command
def check_status():
    '''Check application's status'''
    print('### Extensions:')
    pprint(app.extensions)
    print('-' * 80)

    print('### Modules:')
    pprint(app.blueprints)
    print('-' * 80)

    print('### URL Map:')
    pprint(app.url_map)
    print('-' * 80)

    print('### App:')
    pprint(app)
    print('-' * 80)


@manager.command
def show_config():
    '''Print application's configurations'''
    print('### Config:')
    pprint(dict(app.config))


if __name__ == '__main__':
    manager.run()
