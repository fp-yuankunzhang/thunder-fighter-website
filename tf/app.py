import os
import logging

from logging.handlers import RotatingFileHandler

from flask import Flask, request, jsonify
from werkzeug.contrib.profiler import ProfilerMiddleware

from .filters import add_html_filters


def create_app(config):
    '''
    Create the application instance.

    :param config: The configuration object.
    '''
    app = Flask(__name__)

    config_application(app, config)
    config_error_handlers(app)
    config_extensions(app)
    config_before_handlers(app)
    config_html_filters(app)
    config_modules(app)
    config_logging(app)

    return app


def config_application(app, config):
    """Configure the application instance."""
    app.config.from_object(config)

    if app.debug and app.config['PROFILE']:
        """Watch running app's profile"""
        app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions=[10])


def config_error_handlers(app):
    @app.errorhandler(403)
    def forbidden(error):
        if request.is_xhr:
            return jsonify(error='403 forbidden')
        # return render_template('error.html', error=error), 403
        return '403 forbidden'

    @app.errorhandler(404)
    def page_not_found(error):
        # return render_template('404.html'), 404
        return '404 page not found'

    @app.errorhandler(405)
    def method_not_allowed(error):
        pass

    @app.errorhandler(500)
    def internal_error(error):
        pass


def config_extensions(app):
    pass


def config_before_handlers(app):
    pass


def config_html_filters(app):
    add_html_filters(app)


def config_modules(app):
    for module, url_prefix in app.config['MODULES']:
        app.register_blueprint(module, url_prefix=url_prefix)


def config_logging(app):
    formatter = logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s '
        '[in %(pathname)s:%(lineno)d]'
    )

    debug_log = os.path.join(app.root_path, app.config['DEBUG_LOG'])
    debug_file_handler = \
        RotatingFileHandler(debug_log,
                            maxBytes=100000,
                            backupCount=10)

    debug_file_handler.setLevel(logging.DEBUG)
    debug_file_handler.setFormatter(formatter)
    app.logger.addHandler(debug_file_handler)

    error_log = os.path.join(app.root_path, app.config['ERROR_LOG'])
    error_file_handler = \
        RotatingFileHandler(error_log,
                            maxBytes=100000,
                            backupCount=10)

    error_file_handler.setLevel(logging.ERROR)
    error_file_handler.setFormatter(formatter)
    app.logger.addHandler(error_file_handler)
