# -*- coding: utf-8 -*-
import os

from .modules import (
    frontend,
)

ROOT_DIR = os.path.abspath(os.path.dirname(__file__))


class BaseConfig(object):
    '''Base class for all configuration objects'''

    SITE_NAME = 'Thunder Fighter'
    ADMINS = frozenset(['yuankun.zhang@funplus.com'])

    SECRET_KEY = '%s%s' % (
        '\xe8*AYb\xbd\xaa\x1c\xe3A\xff\xc8E\xa0',
        '\xea\xb0\x82\xc8\x9b\xf7\x8e\x91\xed\x1e'
    )
    SESSION_COOKIE_NAME = 'thunderfighter'

    # for JSON
    JSON_SORT_KEYS = True
    JSONIFY_PRETTYPRINT_REGULAR = True

    MODULES = (
        (frontend.bp, ''),
    )

    # For file upload
    MAX_CONTENT_LENGTH = 8 * 1024 * 1024    # 8M

    # For logging
    DEBUG_LOG = os.path.join(ROOT_DIR, 'logs/debug.log')
    ERROR_LOG = os.path.join(ROOT_DIR, 'logs/error.log')
