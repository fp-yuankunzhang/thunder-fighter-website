#-*- coding: utf-8 -*-
'''
    Developer's local config
'''
from .config import BaseConfig


class LocalConfig(BaseConfig):

    DEBUG = True
    PROFILE = False
